package auth

import (
	"crypto/rand"
	"errors"
	"fmt"
	"os"

	"github.com/golang-jwt/jwt"
)

var signingMethod = jwt.SigningMethodHS256
var secretKey = []byte(os.Getenv("SECRET_JWT_KEY"))

func EncodeJWT(params map[string]string) (string, error) {
	salt := make([]byte, 4)
	rand.Read(salt)
	params["salt"] = string(salt)

	claims := jwt.MapClaims{}

	for key, value := range params {
		claims[key] = value
	}

	token := jwt.NewWithClaims(signingMethod, claims)
	token_str, err := token.SignedString(secretKey)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return token_str, nil
}

func DecodeJWT(tokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, getSecretKey)

	if err != nil {
		return nil, errors.New("invalid token")
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, errors.New("invalid claims")
	}

}

func getSecretKey(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
	}

	return secretKey, nil
}
