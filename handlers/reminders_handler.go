package handlers

import (
	"friendly-chronos-bot/database"
	"friendly-chronos-bot/jobs"
	"friendly-chronos-bot/models"
	"friendly-chronos-bot/views"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

var RemindersListCommand = "reminders_list"

const CreateReminderAction = "create_reminder"

func CreateReminder(tguser *tgbotapi.User, data WebAppAction) (models.Reminder, error) {
	reminder := models.Reminder{UserID: tguser.ID, Question: data.Question, Chron: data.Cron}
	err := reminder.Validate()
	if err != nil {
		return models.Reminder{}, err
	}

	database.DB.Save(&reminder)
	_, err = views.CreateReminderMessage(reminder, err)
	if err != nil {
		return models.Reminder{}, err
	}

	err = createJob(reminder)
	if err != nil {
		return models.Reminder{}, err
	}

	return reminder, nil
}

func RemindersList(tguser *tgbotapi.User) {
	var reminders []models.Reminder
	database.DB.Where("user_id = ?", tguser.ID).Find(&reminders)

	views.RemindersListView(tguser.ID, reminders)
}

func createJob(reminder models.Reminder) error {
	return jobs.CreateReminderJob(reminder)
}
