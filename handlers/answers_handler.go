package handlers

import (
	"fmt"
	"friendly-chronos-bot/database"
	"friendly-chronos-bot/models"
	"friendly-chronos-bot/views"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

func RecordAnswer(message *tgbotapi.Message) {
	result := database.DB.
		Model(&models.Answer{}).
		Where("message_id = ?", message.ReplyToMessage.MessageID).
		Update("text", message.Text)

	if result.Error != nil {
		fmt.Println(result.Error.Error())
	}

	if result.RowsAffected != 1 {
		fmt.Println("found ", result.RowsAffected, " answers")
	}

	views.MarkAnswerRecieved(message)
}

func GetAnswers(reminderID uint) []models.Answer {
	var answers []models.Answer

	result := database.DB.
		Model(&models.Answer{}).
		Where("reminder_id = ?", reminderID).
		Find(&answers)

	if result.Error != nil {
		fmt.Println(result.Error.Error())
	}

	return answers
}
