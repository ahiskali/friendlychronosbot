package handlers

import (
	"encoding/json"
	"errors"
	"log"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

type WebAppAction struct {
	Action   string `json:"action"`   // create_reminder
	Timezone string `json:"timezone"` // Europe/Moscow
	Cron     string `json:"cron"`
	Question string `json:"question"`
}

func ProcessWebAppAction(message *tgbotapi.Message) error {
	var data WebAppAction

	err := json.Unmarshal([]byte(message.WebAppData.Data), &data)

	if err != nil {
		log.Println("Can't read JSON: ", message.WebAppData.Data)
	}

	switch data.Action {
	case CreateReminderAction:
		tguser := message.From
		if tguser == nil {
			return errors.New("user not found")
		}
		_, err = CreateReminder(tguser, data)
	}

	return err
}
