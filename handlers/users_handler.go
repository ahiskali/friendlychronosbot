package handlers

import (
	"errors"
	"friendly-chronos-bot/database"
	"friendly-chronos-bot/models"
	"friendly-chronos-bot/views"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

var StartCommand = "start"

func CreateNewUser(update tgbotapi.Update) (models.User, error) {
	tguser := update.SentFrom()

	if tguser == nil {
		return models.User{}, errors.New("user not found")
	}

	user := models.User{ID: tguser.ID, Username: tguser.UserName}

	database.DB.Create(&user)

	welcome_message, err := views.WelcomeMessage(user)

	if err != nil {
		// Note that panics are a bad way to handle errors. Telegram can
		// have service outages or network errors, you should retry sending
		// messages or more gracefully handle failures.
		panic(err)
	}

	user.LastMessageId = welcome_message.MessageID
	database.DB.Save(&user)

	return user, nil
}
