package helpers

import (
	"friendly-chronos-bot/handlers"
	"time"
)

type Event struct {
	Start string `json:"start"`
	End   string `json:"end"`
	ID    uint   `json:"id"`
	Text  string `json:"text"`
}

func GetAnswersAsEvents(reminderID uint) []Event {
	answers := handlers.GetAnswers(reminderID)

	events := []Event{}

	for _, answer := range answers {
		event := Event{
			Start: answer.CreatedAt.Format(time.RFC3339),
			End:   answer.CreatedAt.Format(time.RFC3339),
			ID:    answer.ID,
			Text:  answer.Text,
		}
		events = append(events, event)
	}

	return events
}
