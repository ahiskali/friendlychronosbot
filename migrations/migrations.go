package migration

import (
	"friendly-chronos-bot/database"
	"log"

	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

func Migrate() {
	db := database.DB

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{{
		// create `users` table
		ID: "20230929045053",
		Migrate: func(tx *gorm.DB) error {
			// it's a good pratice to copy the struct inside the function,
			// so side effects are prevented if the original struct changes during the time
			type user struct {
				gorm.Model
				ID            int64  `gorm:"primarykey"`
				Username      string `gorm:"not null;default: null"`
				LastMessageId int    `gorm:"deafult:null"`
			}

			return tx.Migrator().CreateTable(&user{})
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Migrator().DropTable("users")
		},
	}, {
		// create `reminders` table
		ID: "20230929045553",
		Migrate: func(tx *gorm.DB) error {
			type reminder struct {
				gorm.Model
				Question string
				Chron    string
				Hours    int32
				JobID    string
			}

			return tx.Migrator().CreateTable(&reminder{})
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Migrator().DropTable("reminders")
		},
	}, {
		// add `user_id` column to `reminders` table
		ID: "201608301415",
		Migrate: func(tx *gorm.DB) error {
			type user struct {
				ID int64
			}

			type reminder struct {
				UserID int64
				User   user
			}
			return tx.Migrator().AddColumn(&reminder{}, "UserID")
		},
		Rollback: func(tx *gorm.DB) error {
			type user struct {
				ID int64
			}

			type reminder struct {
				UserID int64
				User   user
			}
			return db.Migrator().DropColumn(&reminder{}, "UserID")
		},
	}, {
		// create `answers` table
		ID: "20230929054410",
		Migrate: func(tx *gorm.DB) error {
			type answer struct {
				gorm.Model
				MessageID int    `gorm:"index"`
				Text      string `gorm:"text"`
			}

			return tx.Migrator().CreateTable(&answer{})
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Migrator().DropTable("answers")
		},
	}, {
		// add `reminder_id` column to `answers` table
		ID: "20230929060815",
		Migrate: func(tx *gorm.DB) error {
			type reminder struct {
				gorm.Model
			}

			type answer struct {
				ReminderID uint
				Reminder   reminder
			}
			return tx.Migrator().AddColumn(&answer{}, "ReminderID")
		},
		Rollback: func(tx *gorm.DB) error {
			type reminder struct {
				gorm.Model
			}

			type answer struct {
				ReminderID uint
				Reminder   reminder
			}
			return db.Migrator().DropColumn(&answer{}, "ReminderID")
		},
	}})

	log.Println("running migrations")
	// m.RollbackTo("201608301415")
	// m.RollbackLast()
	if err := m.Migrate(); err != nil {
		log.Fatalf("Migration failed: %v", err)
	}
	log.Println("Migration did run successfully")
}
