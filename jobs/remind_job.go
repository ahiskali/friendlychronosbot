package jobs

import (
	"fmt"
	"friendly-chronos-bot/database"
	"friendly-chronos-bot/models"
	"friendly-chronos-bot/views"
	"log"
	"time"

	"github.com/go-co-op/gocron"
)

var Scheduler *gocron.Scheduler

func init() {
	Scheduler = gocron.NewScheduler(time.UTC)
	Scheduler.StartAsync()
	CreateJobsForAllReminders()
}

func CreateJobsForAllReminders() {
	reminders := []models.Reminder{}
	result := database.DB.Find(&reminders)

	if result.Error != nil {
		fmt.Println(result.Error.Error())
	}

	for _, reminder := range reminders {
		CreateReminderJob(reminder)
	}
}

func CreateReminderJob(reminder models.Reminder) error {
	job, err := Scheduler.Cron(reminder.Chron).Do(AskQuestion, reminder)
	if err != nil {
		log.Fatal(err.Error())
	}

	job.Tag(fmt.Sprint(reminder.ID))
	log.Println("created job for reminder ", reminder.ID)
	return nil
}

func AskQuestion(reminder models.Reminder) error {
	views.AskQuestionMessage(reminder)
	return nil
}
