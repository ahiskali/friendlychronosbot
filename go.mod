module friendly-chronos-bot

go 1.21

require (
	github.com/enescakir/emoji v1.0.0
	github.com/go-co-op/gocron v1.34.0
	github.com/go-gormigrate/gormigrate/v2 v2.1.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/lnquy/cron v1.1.1
	github.com/matterbridge/telegram-bot-api/v6 v6.5.0
	gorm.io/driver/postgres v1.5.2
	gorm.io/gorm v1.25.4
)

require (
	github.com/google/uuid v1.3.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgx/v5 v5.3.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
