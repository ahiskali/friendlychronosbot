package config

import "os"

var Domain = os.Getenv("DOMAIN")
var PublicCert = os.Getenv("PUBLIC_CERTIFICATE_LOCATION")
var PrivateCert = os.Getenv("PRIVATE_CERTIFICATE_LOCATION")
