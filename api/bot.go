package api

import (
	"os"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

var Bot *tgbotapi.BotAPI

func init() {
	api_token := os.Getenv("TELEGRAM_APITOKEN")
	bot, err := tgbotapi.NewBotAPI(api_token)
	if os.Getenv("BOT_DEBUG") != "" {
		bot.Debug = true
	}
	Bot = bot

	if err != nil {
		panic(err)
	}
}
