# syntax=docker/dockerfile:1

FROM golang:1.21.0 as base
FROM base as dev
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod tidy
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /bin/chronos-bot cmd/main.go
CMD ["/bin/chronos-bot"]


# COPY go.mod go.sum ./
# RUN go mod tidy

# COPY . .
# RUN CGO_ENABLED=0 GOOS=linux go build -o ./chronos-bot cmd/main.go
# CMD ["./chronos-bot"]
