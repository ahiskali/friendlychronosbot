const eventsPath = '/get_events'

async function getEvents() {
  const response = await fetch(eventsPath + window.location.search)
  const events = await response.json()
  return events
}

const dp = new DayPilot.Calendar("dp", {
  viewType: "Week",
  locale: navigator.language,
  timeFormat: "Clock24Hours"
});

// dp.events.list = [
//   {
//     "start": "2023-10-02T10:30:00",
//     "end": "2023-10-02T13:30:00",
//     "id": "225eb40f-5f78-b53b-0447-a885c8e92233",
//     "text": "Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1Calendar Event 1"
//   },
//   {
//     "start": "2023-10-03T12:30:00",
//     "end": "2023-10-03T15:00:00",
//     "id": "1f67def5-e1dd-57fc-2d39-eb7a5f8e789a",
//     "text": "Calendar Event 2"
//   },
//   {
//     "start": "2023-10-04T10:30:00",
//     "end": "2023-10-04T16:00:00",
//     "id": "aba78fd9-09d0-642e-612d-0e7e002c29f5",
//     "text": "Calendar Event 3"
//   }
// ];

dp.init();
getEvents().then((events) => {
  dp.update({events: events});
});

const nav = new DayPilot.Navigator("nav", {
  showMonths: 1,
  skipMonths: 1,
  selectMode: "Week",
  onTimeRangeSelected: args => {
    dp.update({
      startDate: args.day
    });
    console.log(args);
  }
});

nav.init();

Telegram.WebApp.expand()
Telegram.WebApp.ready()
