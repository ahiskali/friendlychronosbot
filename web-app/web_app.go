package web_app

import (
	"encoding/json"
	"errors"
	"fmt"
	"friendly-chronos-bot/auth"
	"friendly-chronos-bot/config"
	"friendly-chronos-bot/database"
	"friendly-chronos-bot/helpers"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

const (
	publicCert     = "/app/web-app/certificates/fullchain.pem"
	privateCert    = "/app/web-app/certificates/privkey.pem"
	TokenParamName = "token"
)

type getEventsParams struct {
	// This should contain the expected JSON data
	// <key> <data type> `json: <where to find the data in the JSON` in this case we have
	UserID     int64 `json:"user_id"`
	ReminderID uint  `json:"reminder_id"`
}

func parseGetEventsParams(query url.Values) (getEventsParams, error) {
	param_string, present := query[TokenParamName]

	if !present {
		return getEventsParams{}, errors.New("bad request: token not present")
	}

	params, err := auth.DecodeJWT(param_string[0])
	if err != nil {
		return getEventsParams{}, fmt.Errorf("bad request: %w", err)
	}
	user_id, err := strconv.ParseInt(params["user_id"].(string), 10, 64)
	if err != nil {
		return getEventsParams{}, fmt.Errorf("bad request: %v", err)
	}

	reminder_id, err := strconv.ParseInt(params["reminder_id"].(string), 10, 32)
	if err != nil {
		return getEventsParams{}, fmt.Errorf("bad request: %v", err)
	}
	return getEventsParams{UserID: user_id, ReminderID: uint(reminder_id)}, nil
}

func getEventsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params, err := parseGetEventsParams(r.URL.Query())

	if err != nil {
		error_text := fmt.Sprintf("Error processing request: %v", err)
		fmt.Println(error_text)
		http.Error(w, error_text, http.StatusBadRequest)
		return
	}

	exists, err := checkIfUserHasreminder(params.UserID, params.ReminderID)

	if err != nil {
		error_text := fmt.Sprintf("internal server error, %v", err)
		fmt.Println(error_text)
		http.Error(w, error_text, http.StatusInternalServerError)
		return
	}

	if !exists {
		error_text := fmt.Sprintf("user %d does not have a reminder %d", params.UserID, params.ReminderID)
		fmt.Println(error_text)
		http.Error(w, error_text, http.StatusForbidden)
		return
	}

	events := helpers.GetAnswersAsEvents(params.ReminderID)
	json.NewEncoder(w).Encode(events)
}

func checkIfUserHasreminder(userID int64, reminderID uint) (bool, error) {
	var exists bool = false

	err := database.DB.Raw(
		"SELECT EXISTS(SELECT 1 FROM reminders WHERE id = ? AND user_id = ?)",
		reminderID,
		userID).
		Scan(&exists).Error

	if err != nil {
		return false, err
	}

	return exists, nil
}

func Init() {
	port := os.Getenv("HTTP_PORT")
	if port == "" {
		port = "80"
	}

	http_mux := http.NewServeMux()
	http_mux.Handle("/", http.RedirectHandler(config.Domain, http.StatusSeeOther))
	go http.ListenAndServe(":http", http_mux)

	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("/app/web-app/assets"))
	mux.HandleFunc("/get_events", getEventsHandler)
	mux.Handle("/", fs)

	go http.ListenAndServeTLS(":https", config.PublicCert, config.PrivateCert, mux)
	fmt.Println("Server started")
}
