package views

import (
	"fmt"
	"friendly-chronos-bot/api"

	"github.com/enescakir/emoji"
	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

func MarkAnswerRecieved(message *tgbotapi.Message) {
	msg := tgbotapi.NewMessage(message.From.ID, emoji.CheckMarkButton.String())
	msg.ReplyToMessageID = message.MessageID
	_, err := api.Bot.Send(msg)

	if err != nil {
		fmt.Println(err.Error())
	}
}
