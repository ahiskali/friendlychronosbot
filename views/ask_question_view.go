package views

import (
	"friendly-chronos-bot/api"
	"friendly-chronos-bot/database"
	"friendly-chronos-bot/models"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

func AskQuestionMessage(reminder models.Reminder) (tgbotapi.Message, error) {
	msg := tgbotapi.NewMessage(reminder.UserID, reminder.Question)
	sent_msg, err := api.Bot.Send(msg)
	answer := models.Answer{MessageID: sent_msg.MessageID, ReminderID: reminder.ID}
	database.DB.Create(&answer)
	database.DB.Save(&answer)

	if err != nil {
		return tgbotapi.Message{}, err
	}

	return sent_msg, nil
}
