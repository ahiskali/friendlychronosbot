package views

import (
	"fmt"
	"friendly-chronos-bot/api"
	"friendly-chronos-bot/models"

	cron "github.com/lnquy/cron"
	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

const createReminderMessageText = "Created reminder with question \"%s\". It will run \"%s\""
const createReminderErrorMessageText = "Something went wrong: "

var exprDesc, _ = cron.NewDescriptor(
	cron.Use24HourTimeFormat(true),
)

// var createReminderKeyboard = tgbotapi.NewInlineKeyboardMarkup(
// 	tgbotapi.NewInlineKeyboardRow(
// 		tgbotapi.NewInlineKeyboardButtonData("0:00", "0"),
// 		tgbotapi.NewInlineKeyboardButtonData("01:00", "1"),
// 		tgbotapi.NewInlineKeyboardButtonData("02:00", "2"),
// 		tgbotapi.NewInlineKeyboardButtonData("03:00", "3"),
// 		tgbotapi.NewInlineKeyboardButtonData("04:00", "4"),
// 		tgbotapi.NewInlineKeyboardButtonData("05:00", "5"),
// 		tgbotapi.NewInlineKeyboardButtonData("06:00", "6"),
// 		tgbotapi.NewInlineKeyboardButtonData("07:00", "7"),
// 	),
// )

func CreateReminderMessage(reminder models.Reminder, err error) (tgbotapi.Message, error) {
	cron_description, _ := exprDesc.ToDescription(reminder.Chron, cron.Locale_en)

	var msg tgbotapi.MessageConfig

	if err != nil {
		msg = tgbotapi.NewMessage(reminder.UserID, fmt.Sprint(createReminderErrorMessageText, err.Error()))
	} else {
		msg = tgbotapi.NewMessage(reminder.UserID, fmt.Sprintf(createReminderMessageText, reminder.Question, cron_description))
	}
	// msg.ReplyMarkup = createReminderKeyboard
	// msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(false)
	sent_msg, err := api.Bot.Send(msg)

	if err != nil {
		// Note that panics are a bad way to handle errors. Telegram can
		// have service outages or network errors, you should retry sending
		// messages or more gracefully handle failures.
		return tgbotapi.Message{}, err
	}

	return sent_msg, nil
}
