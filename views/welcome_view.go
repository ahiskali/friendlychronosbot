package views

import (
	"fmt"
	"friendly-chronos-bot/api"
	"friendly-chronos-bot/config"
	"friendly-chronos-bot/models"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

const webAppKeyboardText = "Create a reminder"

var webAppLink = tgbotapi.WebAppInfo{URL: fmt.Sprintf("https://%s/create_reminder.html", config.Domain)}
var createReminderKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButtonWebApp(webAppKeyboardText, webAppLink),
	),
)

const welcomeMessageText = "Welcome! You can create a reminder using the button below"

func WelcomeMessage(user models.User) (tgbotapi.Message, error) {
	msg := tgbotapi.NewMessage(user.ID, welcomeMessageText)
	msg.ReplyMarkup = createReminderKeyboard
	sent_msg, err := api.Bot.Send(msg)

	if err != nil {
		// Note that panics are a bad way to handle errors. Telegram can
		// have service outages or network errors, you should retry sending
		// messages or more gracefully handle failures.
		return tgbotapi.Message{}, err
	}
	user.LastMessageId = sent_msg.MessageID

	return sent_msg, nil
}
