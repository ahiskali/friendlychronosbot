package views

import (
	"fmt"
	"friendly-chronos-bot/api"
	"friendly-chronos-bot/auth"
	"friendly-chronos-bot/config"
	"friendly-chronos-bot/models"
	"net/url"
	"unicode/utf16"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

const ReminderViewURL = "/daypilot.html"

func RemindersListView(user_id int64, reminders []models.Reminder) (tgbotapi.Message, error) {
	msg_text := ""
	var url_entities []tgbotapi.MessageEntity

	for _, reminder := range reminders {
		url_params := map[string]string{
			"user_id":     fmt.Sprint(user_id),
			"reminder_id": fmt.Sprint(reminder.ID),
		}
		token, _ := auth.EncodeJWT(url_params)
		u := url.URL{
			Scheme: "https",
			Host:   config.Domain,
			Path:   ReminderViewURL,
		}
		params := u.Query()
		params.Set("token", token)
		u.RawQuery = params.Encode()

		questionRune := utf16.Encode([]rune(reminder.Question))
		entity := tgbotapi.MessageEntity{
			Type:   "text_link",
			Offset: len(msg_text),
			Length: len(questionRune),
			URL:    u.String(),
		}
		question := string(utf16.Decode(questionRune))
		url_entities = append(url_entities, entity)
		msg_text += fmt.Sprintln(question)
	}

	msg := tgbotapi.NewMessage(user_id, msg_text)
	msg.Entities = url_entities
	sent_msg, err := api.Bot.Send(msg)

	if err != nil {
		return tgbotapi.Message{}, err
	}

	return sent_msg, nil
}
