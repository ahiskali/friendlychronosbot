package models

import tgbotapi "github.com/matterbridge/telegram-bot-api/v6"

var DefaultScope = tgbotapi.NewBotCommandScopeDefault()

var StartCommand = tgbotapi.BotCommand{
	Command:     "/start",
	Description: "Create new user",
}

var NewReminderCommand = tgbotapi.BotCommand{
	Command:     "/new_reminder",
	Description: "Create new reminder",
}

var UserCommands = tgbotapi.SetMyCommandsConfig{
	Commands:     []tgbotapi.BotCommand{StartCommand, NewReminderCommand},
	Scope:        &DefaultScope,
	LanguageCode: "en",
}
