package models

import (
	"errors"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID            int64  `gorm:"unique;primaryKey"`
	Username      string `gorm:"not null;default: null"`
	LastMessageId int    `gorm:"deafult:null"`
	Reminders     []Reminder
}

type Reminder struct {
	gorm.Model
	UserID   int64
	User     User
	Question string
	Chron    string
	Hours    int32
	JobID    string
	Answers  []Answer
}

func (reminder *Reminder) Validate() error {
	if reminder.Question == "" {
		return errors.New("question can't be empty")
	}

	return nil
}

type Answer struct {
	gorm.Model
	MessageID  int `gorm:"index"`
	ReminderID uint
	Reminder   Reminder
	Text       string `gorm:"text"`
}

func (reminder *Reminder) ToggleHour(hour int) {
	reminder.Hours ^= 1 << hour
}

func (reminder *Reminder) GetHour(hour int) bool {
	return reminder.Hours&(1<<hour) != 0
}
