package main

import (
	"friendly-chronos-bot/api"
	handlers "friendly-chronos-bot/handlers"
	migration "friendly-chronos-bot/migrations"
	web_app "friendly-chronos-bot/web-app"

	tgbotapi "github.com/matterbridge/telegram-bot-api/v6"
)

func main() {
	bot := api.Bot

	web_app.Init()

	migration.Migrate()

	// Create a new UpdateConfig struct with an offset of 0. Offsets are used
	// to make sure Telegram knows we've handled previous values and we don't
	// need them repeated.
	updateConfig := tgbotapi.NewUpdate(0)

	// Tell Telegram we should wait up to 30 seconds on each request for an
	// update. This way we can get information just as quickly as making many
	// frequent requests without having to send nearly as many.
	updateConfig.Timeout = 30

	// Start polling Telegram for updates.
	updates := bot.GetUpdatesChan(updateConfig)

	// Let's go through each update that we're getting from Telegram.
	for update := range updates {
		// Telegram can send many types of updates depending on what your Bot
		// is up to. We only want to look at messages for now, so we can
		// discard any other updates.

		command := update.Message.Command()
		switch command {
		case handlers.StartCommand:
			handlers.CreateNewUser(update)
		case handlers.RemindersListCommand:
			handlers.RemindersList(update.SentFrom())
		}

		if command == "" {
			if update.Message.ReplyToMessage != nil {
				handlers.RecordAnswer(update.Message)
			} else if update.Message.WebAppData != nil {
				handlers.ProcessWebAppAction(update.Message)
			} else {
				// Mirror message
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
				msg.ReplyToMessageID = update.Message.MessageID
				if _, err := bot.Send(msg); err != nil {
					panic(err)
				}
			}
		}
	}

}

type Message struct {
	Value string `json:"value"`
}
